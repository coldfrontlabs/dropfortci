# Dropfort CI Templates

Build, test and deploy Drupal web applications using GitLab CI.

## Stages

### Code Test

This stage runs a series of automated tests on the quality of the code, coding standards and general structure of the application.

Any failures at this stage will prevent the build from continuing and trigger the [Code Test Failure](#code-test-failure) stage.

For more information on individual jobs, see the following:

- [Drupal Code Test](/docs/code-test.drupal.md)
- [Scripts Code Test](/docs/code-test.scripts.md)
- [Styles Code Test](/docs/code-test.styles.md)

### Code Test Failure

This stage will generate a build artifact with any automated fixes applied to code. Developers can then download the artifacts and commit the changes to the repository if they're acceptable.

Any failure in the [Code Test](#code-test) stage triggers tasks in this stage.

For more information on individual jobs, see the following:

- [Drupal Code Test](/docs/code-test.drupal.md#test-failure)
- [Scripts Code Test](/docs/code-test.scripts.md#test-failure)
- [Styles Code Test](/docs/code-test.styles.md#test-failure)

### Build

This stage will run various package manager installers such as composer and npm to gather all required dependencies, as well as compile any code that cannot be rendered or run as-is.

Once code is compiled and ready, an artifact will be saved for a set amount of time for future stages to use.

For more information on individual jobs, see the following:

- [Drupal Build](/docs/build.drupal.md)
- [Vue Build](/docs/build.vue.md)

### Test

This stage is reserved tests which require the code base to be available but don't necessarily require a deployed copy of the web application. For example unit test can be performed here.
You could also perform fresh install tests of the application using local webservers (e.g. php webserver) and local databases (e.g. sqlite). No actual application data is expected to be required for these tests.

### Pre Deploy

This is an optional stage which is triggered when certain environment variables are set. This is used to initially setup the web application for the first time. It will deploy the code, set the database credentials and perform a fresh install.

**This stage will destroy any data on the remote target.**

Set the following variables to trigger this stage:

- `DF_INITIAL_SETUP=true`

For more information on individual jobs, see the following:

- [Drupal Pre Deploy](/docs/deploy.drupal.md#pre-deploy)

### Deploy

The standard deploy phase. This is responsible for moving the code from the build server to your remote servers. It will target your dev, qa, uat and/or production server based on the build type and git branch/tag that triggered the build.

**Even though the deploy creates local backup to rollback the changes it is not considered a long term backup solution. It is recommended to have other backups available in case issues arise during the deploy process.**

For more information on individual jobs, see the following:

- [Drupal Deployment](/docs/deploy.drupal.md)
- [Vue Deployment](/docs/deploy.vue.md)

### Update Data

This stage is used for tasks such as database updates,  migrations or other tasks related to data changes which require the new code to already be in place.

For more information on individual jobs, see the following:

- [Update Drupal Data](/docs/update.drupal.md)

### Validate

The validation stage is responsible for checking the quality and consistency of the deployment. It runs several tests on the deployed application to confirm it was deployed properly.
This is different from the *Test* phase as these tests are performed on a live remote site.

The following tests are available:

- Automated acceptance tests using Codeception and web driver
- Visual regression tests using BackstopJS
- Accessibility tests using Lighthouse and pa11y-ci
- Available Drupal security updates through Drush
- Available NPM sercurity update through NPM

See the `testing` folder in your code repository for details on configuring test cases as well as the `package.json` file for triggering tests.

**Failures during the validation phase do not rollback the deployment.** Rolling back changes after this point requies a new deployment or manual intervention.

### Cleanup

The last step in the deployment is to cleanup the temporary files and older backups, and perform any final reporting on the deployment.

The phase also contains several manual jobs available for copying data and files to other environments.

For more information on individual jobs, see the following:

- [Drupal Backcopy](/docs/backcopy.drupal.md)
- [Drupal Cleanup](/docs/cleanup.drupal.md)
- [Vue Cleanup](/docs/cleanup.vue.md)

## Templates

## Testing Dropfort CI Scripts

This project triggers builds on the Drupal Template project to load these files for testing.
