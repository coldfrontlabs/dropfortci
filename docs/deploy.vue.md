# Vue Deployment

Moves the code from the build server to your remote servers. It will target a server based on the `CI_TLD` value, build type and git branch/tag that triggered the build.

Then the code is copied over using rsync where it will:

- Update files which have changed since the last deployment
- Delete files which are no longer in the build

 > Even though the deploy creates local backup to rollback the changes it is not considered a long term backup solution. It is recommended to have other backups available in case issues arise during the deploy process.

## Customization

All deploy phases can (and should) be customized using environment variables during the CI/CD process.

### Deploying to different environments

Unlike Drupal, Vue does not have a site alias file to draw upon. You must set the `CI_TLD` variable for each deploy phase to ensure the code gets deployed to the correct server.
