# Vue Build

> These jobs assume you have created a `package.json` file with a script called `build:prod`, `build:qa` and `build:dev` inside of your project.

There are 3 kinds of build phases for Vue projects: Development, QA, and Release.

All build phases always perform the following two actions:

- `npm install`
- `npm build`

The options passed to the `build` command vary slightly between the development, QA, and release builds but the result is the same; the application codebase is downloaded and compiled.

All build phases are identical in function with the following exceptions:

- Development and QA build artifacts expire in 24hrs
- Release builds are kept forever

## Customization

All build phases can be customized using environment variables during the CI/CD process.

## Changing the build command

The `BUILD_NPM_OPTIONS` variable can be set to change which build command is used.

By default, the variable is set to:

| Environment | Variable Value | Resulting command |
| --- | --- | --- |
| Development | dev | `npm run build:dev` |
| QA | qa | `npm run build:qa` |
| Production | prod | `npm run build:prod` |
