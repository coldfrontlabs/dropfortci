# Drupal Cleanup

This job ensures only a single local backup of the Drupal site is saved on the server.

## Customization

### Changing the backup sub-directory

If you have customized the backup sudirectory using the provided archive script found in the Dropfort Drupal Project Template, you will want to do the same in this step by using the `CLEANUP_ENVIRONMENT` variables.

By default, this variable is set to the value of `default` just like the archive script.
