# Drupal Deployment

Moves the code from the build server to your remote servers. It will target your dev, qa, uat and/or production server based on the build type and git branch/tag that triggered the build.

The remote site is put into maintenance mode prior to the deployment. Then the code is copied over using rsync where it will:

- Update files which have changed since the last deployment
- Delete files which are no longer in the build
  - with the exception of configuration files in sites/* directory
  - with the exception of media files in the public files directory

After the deployment, the file permissions are restored. The site remains in offline mode until the next phase.

 > Even though the deploy creates local backup to rollback the changes it is not considered a long term backup solution. It is recommended to have other backups available in case issues arise during the deploy process.

## Pre Deploy

The pre-deploy stage will deploy the code, set the database credentials and perform a fresh install.

> This stage will destroy any data on the remote target.

Set the following variables to trigger this stage:

- `DF_INITIAL_SETUP=true`

 The following options are available to customize the installation

- `DF_INITIAL_PROFILE=minimal`
  - Replace with the name of your desired Drupal installation profile.
- `DF_INITIAL_DB_URL=mysql://dbuser:dbpass@hostname/dbname`
  - Note the database needs to already exist.
- `DF_INTIAL_SITE=default`
  - The name of the folder to use in the `sites` directory. Defaults to **default**.
- `DF_INITIAL_OPTIONS`
  - Any additional options you'd like to pass to the `drupal si` command. See `drush si --help` for more information.
