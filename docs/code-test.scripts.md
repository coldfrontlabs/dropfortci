# Scripts Code Test

> These jobs assume you have created a `package.json` file with a script called `lint:scripts` and `lint:scripts:fix` inside of your project.

The intended purpose of this stage is to run a code test on all JavaScript files within the project.

## Test Failure

If there are errors found within the initial code test stage, a fixer should be run on all JavaScript files found within the project.

The result of these fixes will be saved to a build artifact.
