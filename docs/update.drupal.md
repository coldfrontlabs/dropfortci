# Update Drupal Data

This phase makes the necessary changes to your database as part of the deployment. Primarily it run Drupal update scripts (using `drush updb`) to ensure update hooks are processed.
This phase is also responsible for importing and syncing your site configuration to ensure it is up to date.

The last step in this stage is to put the site back into online mode so users can continue to access the application.
