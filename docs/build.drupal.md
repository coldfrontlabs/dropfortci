# Drupal Build

> These jobs assume you have created a `package.json` file with a script called `build:prod` and `build:dev` inside of your project.

There are 2 kinds of build phases for Drupal projects: Development and Release.

Both build phases always perform the following two actions:

- `composer install`
- `npm run theme:build`

The options passed to those commands may vary slightly between the development and release builds but the result is the same; the application codebase is downloaded and theme assets are compiled.

Both build phases are identical in function with the following exceptions:

- Development builds contain developer tools and test libraries
- Development build artifacts expire in 24hrs
- Release builds are kept forever

Both builds use `composer.json` file and the `package.json` file to download and process the application code. The package.json file in particular has several stock scripts for performing other tasks most commonly associated to the theme of the site. See the *scripts* section in that file for more details.
