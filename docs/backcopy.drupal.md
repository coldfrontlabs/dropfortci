# Drupal Backcopy

These are manually triggered jobs for moving data and files from one site to another.

For example if you wanted to refresh your QA site with production data after the deployment you can trigger the "Backcopy Prod to QA" job.
These jobs are split between the database data and the media files. You can run either independently or both to get everything.

**Note that any copies of production data to other environments are automatically sanitized of real user emails, passwords and other sensitive information using `drush sql-sanitize`**

The stock jobs provide you with the ability to backcopy both data and files from Production to QA or Dev.
