# Drupal Code Test

Runs [PHP Code Sniffer](https://github.com/squizlabs/PHP_CodeSniffer) using the [Drupal coding standards](https://www.drupal.org/docs/develop/standards/coding-standards) on all PHP files found within the project.

For more detailed documenation on PHPCS, please read their [wiki](https://github.com/squizlabs/PHP_CodeSniffer/wiki).

This stage also checks your `composer.json` file for any problems. These failures don't block builds but should be addressed.

## Test Failure

If there are errors found within the initial code test stage, PHP Code Beautifier and Fixer will be run on all PHP files found within the project and the result will be saved to a build artifact.

## Customization

Both the standard code test and code test failure can be customized using environment variables during the CI/CD process.

### Including different file extentions

By default, PHPCS only checks `.php` files. This can lead to `.module` or `.theme` files being ignored by the code check.

You can set multiple file extensions to be included in the code check by setting the `PHPCS_EXTENSTIONS` variable.

An example of this can be found in the [profile.drupal.yml](/templates/profile.drupal.yml#L5) file.

### Ignoring specific files

Typically you don't want to run the code check on Drupal core or modules from contrib.

You can ignore specific files or file patterns during the code check by setting the `PHPCS_IGNORE` variable.

An example of this can be found in the [module.drupal.yml](/templates/module.drupal.yml#L4) file.
