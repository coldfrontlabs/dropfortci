# Styles Code Test

> These jobs assume you have created a `package.json` file with a script called `lint:styles` and `lint:styles:fix` inside of your project.

The intended purpose of this stage is to run a code test on all style files (such as CSS, Sass, or SCSS) within the project.

## Test Failure

If there are errors found within the initial code test stage, a fixer should be run on all style files found within the project.

The result of these fixes will be saved to a build artifact.

## NPM Scripts

See the following examples and resources for creating code testing tasks in npm:

- Drupal Theme Template (Coming soon!)
