# Vue Cleanup

This job ensures only a single local backup of the Vue site is saved on the server.

## Customization

All cleanup phases can (and should) be customized using environment variables during the CI/CD process.

### Cleaning up different environments

Unlike Drupal, Vue does not have a site alias file to draw upon. You must set the `CI_TLD` variable for each cleanup phase to ensure the code gets deployed to the correct server.
